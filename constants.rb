INPUT_LINE = '>'

EXIT_COMMANDS = ['exit', 'q', 'quit', 'stop']

SUPPORTED_OPERATORS = ['+', '-', '*', '/']

HELP_COMMANDS = ['h', 'help']
