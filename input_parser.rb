require './input_validator'
require './calculator'

class InputParser
  attr_accessor :input_validator
  attr_accessor :calculator

  def initialize
    @input_validator = InputValidator.new
    @calculator = Calculator.new
  end

  def parse(input)
    if input == 'stack'
      return calculator.stack
    elsif input == 'clear'
      calculator.clear
      return
    end
    input_tokens = input.split(' ')
    if @input_validator.valid_input?(input_tokens, calculator.operators_on_stack?)
      calculator.stack.add_input_to_stack(input_tokens)
      calculator.calculate_stack
    else
      'Invalid input (enter "h" for help)'
    end
  end

end
