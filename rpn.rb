require './constants'
require './input_parser'
require './help'

puts 'Welcome to the Reverse Polish Notation calculator! Press "h" at any time for help.'
puts INPUT_LINE

input_parser = InputParser.new
loop do
  x = $stdin.gets
  break if x.nil? #$stdin will be nil when Ctrl+D is pressed

  user_input = x.chomp

  break if EXIT_COMMANDS.include?(user_input)

  if HELP_COMMANDS.include?(user_input)
    puts Help.help_message
    next
  end

  puts input_parser.parse(user_input)

  puts INPUT_LINE
end

