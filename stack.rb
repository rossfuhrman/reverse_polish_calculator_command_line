require './constants'
class Stack
  attr_accessor :operands, :operators

  def initialize
    @operands = []
    @operators = []
  end

  def add_input_to_stack(validated_tokens)
    validated_tokens.each do |token|
      if SUPPORTED_OPERATORS.include?(token)
        @operators << token
      else
        @operands << Float(token)
      end
    end
  end

  def to_s
    "The current stack is: #{@operands.join ' '} #{@operators.join ' '}"
  end

end
