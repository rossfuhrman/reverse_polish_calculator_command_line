require './constants'

class InputValidator
  def valid_input?(input_tokens, operators_already_on_stack)
    new_operators = []
    input_tokens.each do |token|
      if SUPPORTED_OPERATORS.include?(token)
        return false if operators_already_on_stack
        new_operators << token
      else
        return false if invalid_operand?(token, new_operators)
      end
    end
    true
  end

  private 

  def invalid_operand?(token, new_operators)
    begin
      operators_exist = new_operators.length > 0
      return true if operators_exist
      Float(token) 
    rescue ArgumentError 
      return true
    end
    false
  end
end
