require './input_validator'

describe InputValidator do
  let :input_validator { InputValidator.new }

  describe '#valid_input?' do

    context 'single operand' do
      it 'is valid' do
        expect(input_validator.valid_input?( ['1'], false )).to be true
      end
    end

    context 'two operands and an operator' do
      it 'is valid ' do
        expect(input_validator.valid_input?( ['1', '2', '+'], false )).to be true
      end
    end

    context 'bad input' do
      it 'is invalid' do
        expect(input_validator.valid_input?( ['junk'], false )).to be false
      end
    end

    context 'input includes operands after the operators' do
      it 'is invalid ' do
        expect( input_validator.valid_input?( ['*', '3'], false) ).to be false
      end
    end

    context 'operators are already on the stack' do
      context 'more operators are provided' do
        it 'is invalid' do
          expect( input_validator.valid_input?( ['3', '4', '*'], operators_alread_on_stack = true) ).to be false
        end
      end
      context 'only operands are provided' do
        it 'is invalid' do
          expect( input_validator.valid_input?( ['3', '4'], operators_alread_on_stack = true) ).to be true
        end
      end
    end
  end

end
