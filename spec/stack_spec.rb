require './stack'

describe Stack do
  let(:stack) { Stack.new }
  describe '#add_input_to_stack' do
    context 'when stack is empty' do
      it 'handles one operand' do
        stack.add_input_to_stack( ['1'] )
        expect(stack.operands).to eql( [1.0] )
        expect(stack.operators).to eql( [] )
      end
      it 'handles two operands' do
        stack.add_input_to_stack( ['1', '2'])
        expect(stack.operands).to eql( [1.0, 2.0] )
        expect(stack.operators).to eql( [] )
      end
      it 'handles two operands and an operator' do
        stack.add_input_to_stack( ['1', '2', '+'] )
        expect(stack.operands).to eql( [1.0, 2.0] )
        expect(stack.operators).to eql( ['+'] )
      end
    end
    context 'when stack is not empty' do
      it 'handles one operand' do
        stack.add_input_to_stack( ['1'] )
        stack.add_input_to_stack( ['2'] )
        expect(stack.operands).to eql( [1.0, 2.0] )
        expect(stack.operators).to eql( [] )
      end
      it 'handles two operands' do
        stack.add_input_to_stack( ['1'] )
        stack.add_input_to_stack( ['2', '3'] )
        expect(stack.operands).to eql( [1.0, 2.0, 3.0] )
        expect(stack.operators).to eql( [] )
      end
      it 'handles two operands and an operator' do
        stack.add_input_to_stack( ['1'] )
        stack.add_input_to_stack( ['2', '3', '+'] )
        expect(stack.operands).to eql( [1.0, 2.0, 3.0] )
        expect(stack.operators).to eql( ['+'] )
      end
      it 'handles three operands and two operators' do
        stack.add_input_to_stack( ['1', '2', '3', '+', '*'] )
        expect(stack.operands).to eql( [1.0, 2.0, 3.0] )
        expect(stack.operators).to eql( ['+', '*'] )
      end
    end
  end
end
