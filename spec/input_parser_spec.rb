require './input_parser'

describe InputParser do
  let :input_parser { InputParser.new }

  describe '#stack' do
    it 'outputs the stack' do
      input_parser.parse('1 +')
      expect(input_parser.parse('stack').to_s).to eql('The current stack is: 1.0 +')
    end
  end
  
  
  describe '#clear' do
    it 'clears out the stack' do
      input_parser.parse('1 +')
      input_parser.parse('clear')
      expect( input_parser.calculator.stack.operands ).to eql( [] )
      expect( input_parser.calculator.stack.operators ).to eql( [] )
    end
  end

  describe '#parse' do

    describe 'validates input' do
      context 'invalid input provided' do
        it 'shows an error message' do
          expect(input_parser.parse('junk')).to eql('Invalid input (enter "h" for help)')
        end
        it 'does not alter the stack' do
          input_parser.parse('1 +')
          input_parser.parse('junk')
          expect(input_parser.calculator.stack.operands).to eql( [1.0] )
          expect(input_parser.calculator.stack.operators).to eql( ['+'] )
        end
      end

      context 'input includes operands after the operators' do
        it 'shows an error message' do
          expect( input_parser.parse('* 3') ).to eql('Invalid input (enter "h" for help)')
        end
      end

      context 'existing operators and more operators are provided' do
        it 'shows an error message' do
          input_parser.parse('+')
          expect( input_parser.parse('3 4 *') ).to eql('Invalid input (enter "h" for help)')
        end
      end
    end

    describe 'no previous_stack' do
      it 'parses one intitial operand' do
        expect( input_parser.parse('1') ).to eql(1.0)
        expect( input_parser.calculator.stack.operands ).to eql( [1.0] )
      end
    end

    describe 'with one operand in previous_stack' do

      before :each do
        input_parser.parse('1')
      end

      it 'parses input of one operand' do
        expect( input_parser.parse('2') ).to eql(2.0)
        expect( input_parser.calculator.stack.operands ).to eql( [1.0, 2.0] )
      end
      it 'parses input of one operand and one operator' do
        expect( input_parser.parse('2 +') ).to eql(3.0)
        expect( input_parser.calculator.stack.operands ).to eql( [3.0] )
      end
      it 'parses input of two operands and one operator' do
        expect( input_parser.parse('2 3 +') ).to eql(5.0)
        expect( input_parser.calculator.stack.operands ).to eql( [1.0, 5.0] )
      end
    end
  end
end
