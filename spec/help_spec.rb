require './help'

describe Help do
  describe "#output help" do
    it 'outputs help' do
      expect(Help.help_message).to match(/This is the help command:/)
    end
  end
end
