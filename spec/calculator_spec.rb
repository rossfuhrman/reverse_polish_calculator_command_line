require './calculator'

describe Calculator do
  let :calculator { Calculator.new }

  describe '#calculate_stack' do

    describe 'handles the operators' do
      it 'can do addition' do
        calculator.stack.operands = [1.0, 2.0] 
        calculator.stack.operators = ['+'] 
        expect( calculator.calculate_stack ).to eql(3.0)
      end

      it 'can do subtraction' do
        calculator.stack.operands = [3.0, 2.0]
        calculator.stack.operators = ['-']
        expect( calculator.calculate_stack ).to eql(1.0)
      end

      it 'can do multiplication' do
        calculator.stack.operands = [4.0, 5.0]
        calculator.stack.operators = ['*']
        expect( calculator.calculate_stack ).to eql(20.0)
      end

      it 'can do division' do
        calculator.stack.operands = [10.0, 2.0]
        calculator.stack.operators = ['/']
        expect( calculator.calculate_stack ).to eql(5.0)
      end
    end

    context 'one operand on the stack' do
      it 'returns the operand' do
        calculator.stack.operands = [1.0]
        expect( calculator.calculate_stack ).to eql(1.0)
      end
    end

    it 'calculates negative operands' do
      calculator.stack.operands = [-1.0, 2.0]
      calculator.stack.operators = ['+']
      expect( calculator.calculate_stack ).to eql(1.0)
      calculator.stack.operands = [-1.0, 2.0]
      calculator.stack.operators = ['*']
      expect( calculator.calculate_stack ).to eql(-2.0)
    end

    it 'calculates many operands and operators at once' do
      calculator.stack.operands = [1.0, 2.0, 3.0, 10.0, 5.0]
      calculator.stack.operators = ['/', '*', '+', '-']
      expect( calculator.calculate_stack ).to eql(-7.0)
    end
  end

  describe '#operators_on_stack?' do
    context 'operators on the stack' do
      it 'returns true' do
        calculator.stack.operators = ['+']
        expect( calculator.operators_on_stack?).to be true
      end
    end

    context 'no operators on the stack' do
      it 'returns false' do
        calculator.stack.operators = []
        expect( calculator.operators_on_stack?).to be false
      end
    end
  end
end
