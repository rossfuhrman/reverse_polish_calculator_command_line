require './constants'

#This implementation is specific to the command line app
#and would need to be modified to be used in a different application.
class Help

  def self.help_message
    <<-HEREDOC
    This is the help command:
    #{HELP_COMMANDS} - Access help at any time
    "stack" - View the current stack
    "clear" - Clear the current stack
    #{SUPPORTED_OPERATORS} - Supported mathematical operators
    #{EXIT_COMMANDS} - Exit the calculator
    SAMPLES:
    #{INPUT_LINE}
    3
    will add 3 to the stack
    #{INPUT_LINE}
    1 2
    will add 1 and 2 to the stack
    #{INPUT_LINE}
    1 2 +
    will add 1 and 2, assuming the stack was empty to begin with
    #{INPUT_LINE}
    HEREDOC
  end

end
