require './stack'

class Calculator
  attr_accessor :stack

  def initialize
    init_new_stack
  end

  def clear
    init_new_stack
  end

  def calculate_stack
    return_value = @stack.operands.last
    while stack_has_items_to_operate_on?
      return_value = process_stack
    end
    return_value
  end

  def operators_on_stack?
    stack.operators.any?
  end

  private

  def init_new_stack
    @stack = Stack.new
  end

  def stack_has_items_to_operate_on?
    @stack.operands.length > 1 && @stack.operators.length > 0
  end

  def process_stack
    operand_pair = @stack.operands.pop(2)
    result = operand_pair.first.send(@stack.operators.shift, operand_pair.last)
    @stack.operands << result
    result
  end

end
