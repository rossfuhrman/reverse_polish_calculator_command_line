##Solution

This is an implementation of a Reverse Polish notation calculator (see [Wikipedia](https://en.wikipedia.org/wiki/Reverse_Polish_notation) )

The calculator is ran from the terminal and supports adding one to many operands and operators at a time. It also has a 'help' command, a 'stack' command to view the current stack', a 'clear' command to clear the calculator, as well as support for quitting.

##Architecture

The `rpn.rb` file is the starting point for using the calculator. However, the majority of the logic for the calculator lives elsewhere. This will make it easier to move the calculator logic to a shared library if it is needed in multiple applications.

##Trade-offs

The `rpn.rb` file has no tests so it has to be tested manually. Ideally, it would have some automated test coverage.

The app provides a variety of ways to quit, but it does not gracefully handle Ctrl+c.

I'm not confident that it correctly handles very large and very small numbers. That is something I would like to add more tests around.

The rpn.rb is the starting point of the app and the only logic-containing class it interacts with is the InputParser. The InputParser initializes a Calculator and calls it. Given this app is a calculator, it seems like the Calculator class should be driving things.

##Running the app

Assuming you have a valid ruby version installed, you can run the app from the terminal with the following command:

`ruby rpn.rb`

This has only been tested with ruby 2.4.1 on a Mac, but it is likely to work on any ruby 2+ version, on any operating system.

##Running the tests

Run the tests:

`rspec spec`
